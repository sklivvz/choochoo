﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;


namespace AddressProcessing
{
    /// <summary>
    /// This class reads and writes TAB-delimited files.
    /// It's a facade over CsvReader and CsvWriter
    /// </summary>
    public class CSVReaderWriter : IDisposable
    {
        public enum Mode { Read = 1, Write = 2 };

        private Mode? _mode;
        private string _filename;
        private StreamReader _csvReader;
        private StreamWriter _csvWriter;
        private bool _disposed = false;
        private const char Separator = '\t';

        /// <summary>
        /// Initializes a new instance of the <see cref="CSVReaderWriter"/> class.
        /// </summary>
        [Obsolete("Please use the constructor with parameters")]
        public CSVReaderWriter()
        {

        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CSVReaderWriter"/> class.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="mode">The mode.</param>
        public CSVReaderWriter(string fileName, Mode mode)
        {
            _mode = mode;
            _filename = fileName;

            SetMode();
        }

        private void SetMode()
        {
            if (_disposed)
                throw new ObjectDisposedException("Resource was disposed.");
            
            // should use a dependency injected factory here... no time

            switch (_mode)
            {
                case Mode.Read:
                    _csvReader = File.OpenText(_filename);
                    break;
                case Mode.Write:
                    var fileInfo = new FileInfo(_filename);
                    _csvWriter = fileInfo.CreateText();
                    break;
                default:
                    throw new InvalidOperationException("Impossible to create a file with that mode.");
            }
        }


        /// <summary>
        /// Sets up the class for reading or writing.
        /// </summary>
        /// <param name="fileName">The name of the file in question</param>
        /// <param name="mode">The mode (read or write).</param>
        [Obsolete("Please use the constructor with parameters")]
        public void Open(string fileName, Mode mode)
        {
            if (_mode.HasValue)
                Close();

            _mode = mode;
            _filename = fileName;

            try
            {
                SetMode();
            }
            catch (InvalidOperationException)
            {
                throw new Exception(); // backwards compatibility
            }
        }

        /// <summary>
        /// Writes the specified columns.
        /// </summary>
        /// <param name="columns">The columns.</param>
        /// <exception cref="System.InvalidOperationException">Trying to write in read mode</exception>
        public void Write(params string[] columns)
        {
            if (_disposed)
                throw new ObjectDisposedException("Resource was disposed."); 
            
            if (!_mode.HasValue || _mode.Value != Mode.Write)
                throw new InvalidOperationException("Trying to write in read mode");

            _csvWriter.WriteLine(string.Join(Separator.ToString(CultureInfo.InvariantCulture), columns));
        }

        /// <summary>
        /// Reads a record.
        /// </summary>
        /// <returns>A IEnumerable of the values read</returns>
        public IEnumerable<string> ReadRecord()
        {
            if (_disposed)
                throw new ObjectDisposedException("Resource was disposed."); 
            
            if (!_mode.HasValue || _mode.Value != Mode.Read)
                throw new InvalidOperationException("Trying to read in write mode");

            var line = _csvReader.ReadLine();
            return line != null ? 
                line.Split(Separator) : 
                Enumerable.Empty<string>();
        }


        /// <summary>
        /// Don't use this method.
        /// </summary>
        /// <param name="column1">The column1.</param>
        /// <param name="column2">The column2.</param>
        /// <returns></returns>
        [Obsolete("Use ReadRecord")]
// ReSharper disable RedundantAssignment
        public bool Read(string column1, string column2)
// ReSharper restore RedundantAssignment
        {
            return Read(out column1, out column2);
        }


        /// <summary>
        /// Reads 2 columns.
        /// </summary>
        /// <param name="column1">The out column 1 content.</param>
        /// <param name="column2">The out column 2 content.</param>
        /// <returns></returns>
        [Obsolete("Use ReadRecord")]
        public bool Read(out string column1, out string column2) // Why does this only support 2 columns? 
        //Could it possibly return a IEnumerable<string>?
        {
            string[] columns = ReadRecord().ToArray();

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            column1 = columns[0];
            column2 = columns[1];

            return true;
        }

        /// <summary>
        /// Closes any open streams.
        /// </summary>
        [Obsolete("Use a using block or call Dispose")]
        public void Close()
        {
            if (_disposed)
                throw new ObjectDisposedException("Resource was disposed."); 
            
            _mode = null;

            if (_csvWriter != null)
            {
                _csvWriter.Close();
            }

            if (_csvReader != null)
            {
                _csvReader.Close();
            }
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            //Not thread safe
            if (!_disposed)
            {
                if (disposing)
                {
                    // these will close the underlying streams
                    if (_csvWriter != null) _csvWriter.Dispose();
                    if (_csvReader != null) _csvReader.Dispose();
                }
                _csvWriter = null;
                _csvReader = null;
                _disposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
