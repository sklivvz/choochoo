﻿using System;
using System.IO;

namespace AddressProcessing
{
    /// <summary>
    /// A junior developer was tasked with writing a reusable solution for an application to read and write text files that hold tab separated data.
    /// His implementation, although it works and meets the needs of the application, is of very low quality.
    /// Your task:
    ///     - Identify and annotate the shortcomings in the current implementation as if you were doing a code review, using comments in this file.
    ///     - In a fresh solution, refactor this implementation into clean,  elegant, rock-solid & well performing code, without over-engineering. 
    ///     - Where you make trade offs, comment & explain.
    ///     - Assume this code is in production and it needs to be backwards compatible. Therefore if you decide to change the public interface, 
    ///       please deprecate the existing methods. Feel free to evolve the code in other ways though.   
    /// </summary>
    /// 

    // General note. 
    // I would firmly encourage the author to use a pre-built CSV library. There are plenty of excellent ones in NuGet.

    // Missing documentation
    public class CSVReaderWriter // TODO: Classes that use disposable resources should be disposable (see FDGL).
    {
        private StreamReader _readerStream = null;
        private StreamWriter _writerStream = null;

        [Flags] // Why flags if we don't support read AND write at the same time?
        // Missing documentation
        public enum Mode { Read = 1, Write = 2 };

        // Missing documentation
        public void Open(string fileName, Mode mode)
        // Breaks the single responsibility principle
        // There should be possibly 2 classes for reading and writing
        {
            if (mode == Mode.Read)
            {
                _readerStream = File.OpenText(fileName);
            }
            else if (mode == Mode.Write)
            {
                FileInfo fileInfo = new FileInfo(fileName);
                _writerStream = fileInfo.CreateText();
            }
            else
            {
                throw new Exception("Unknown file mode for " + fileName); // If we remove [Flags] we can remove this as unreacheable
            }
        }


        // Missing documentation
        public void Write(params string[] columns)
        {
            string outPut = "";

            // use string.Join here, one line :-)

            for (int i = 0; i < columns.Length; i++)
            {
                outPut += columns[i];
                if ((columns.Length - 1) != i)
                {
                    outPut += "\t"; // Why does a CSV Reader Writer use tabs and not commas? :-)

                    // What happens if columns[i] contains a tab character? Should/Could it be encoded somehow?
                }
            }

            //Doesn't check whether the stream is open before writing...
            WriteLine(outPut);
        }

        // Missing documentation
        // Wrong name, or this method doesn't do what it suggests. It should be "HasReadTwoColumns" or similar
        // The parameters are not used and should be dropped
        public bool Read(string column1, string column2)
        {
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' };

            //Doesn't check whether the stream is open before reading...
            line = ReadLine();
            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                // column 1 and column 2 will go out of scope upon return, why set them?
                column1 = null;
                column2 = null;

                return false;
            }
            else
            {
                // column 1 and column 2 will go out of scope upon return, why set them?
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        // Missing documentation
        public bool Read(out string column1, out string column2) // Why does this only support 2 columns? 
        //Could it possibly return a IEnumerable<string>?
        {
            //Seems a bit... unnecessary :-)
            const int FIRST_COLUMN = 0;
            const int SECOND_COLUMN = 1;

            string line;
            string[] columns;

            char[] separator = { '\t' }; // since this is used in two places it could concievable be a class constant.

            //Doesn't check whether the stream is open before reading...
            line = ReadLine();

            //Line is never null, this never happens
            if (line == null)
            {
                column1 = null;
                column2 = null;

                return false;
            }

            columns = line.Split(separator);

            if (columns.Length == 0)
            {
                column1 = null;
                column2 = null;

                return false;
            }
            else // what if there are not 2 columns, but one or five?
            {
                column1 = columns[FIRST_COLUMN];
                column2 = columns[SECOND_COLUMN];

                return true;
            }
        }

        // The following two methods seem to have no purpose, consider inlining them

        private void WriteLine(string line)
        {
            _writerStream.WriteLine(line);
        }

        private string ReadLine()
        {
            return _readerStream.ReadLine();
        }

        // Missing documentation
        public void Close()
        {
            //Should be called from the IDisposable.Dispose() method (see FDGL)
            if (_writerStream != null)
            {
                _writerStream.Close();
            }

            if (_readerStream != null)
            {
                _readerStream.Close();
            }
        }
    }
}
