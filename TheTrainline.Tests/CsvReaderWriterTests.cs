﻿using AddressProcessing;
using NUnit.Framework;
// ReSharper disable EmptyGeneralCatchClause
// ReSharper disable InconsistentNaming
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TheTrainline.Tests
{
    [TestFixture]
    public class CSVReaderWriterTests
    {
        [TearDown]
        public void TearDown()
        {
            foreach (string testFile in TestFiles)
            {
                try
                {
                    File.Delete(testFile);
                }
                catch (Exception)
                {
                    //Running tests in parallel often locks files and this prevents deletion.
                    //Not ideal at all - however this is due to the nature of the original class (no DI)
                    //We need these tests to ensure backwards compatibility.
                    //Remember to clean your temp folder manually
                }
            }
        }

        public List<string> TestFiles = new List<string>();

        public string CreateFile(params string[] contents)
        {
            string testFile = Path.GetTempFileName();
            using (StreamWriter f = File.CreateText(testFile))
            {
                if (contents != null)
                    f.WriteLine(string.Join("\t", contents));
                f.Flush();
                f.Close();
            }
            TestFiles.Add(testFile);

            return testFile;
        }

        public IEnumerable<string[]> GivenInstance_WhenWriteOldFile_ThenSuccess_Data()
        {
            return new[]
                {
                    new[] {"foo"},
                    new[] {"foo", "bar"},
                    new[] {"foo", "bar", "baz"},
                    new[] {""},
                    new[] {"\t"},
                    new string[] {}
                };
        }

        public IEnumerable<Tuple<string[], bool>> GivenInstance_WhenReadOldFile_ThenSuccess_Data()
        {
            return new[]
                {
                    new Tuple<string[], bool>(new[] {"foo", "bar"}, true),
                    new Tuple<string[], bool>(new[] {"foo", "bar", "baz"}, true),
                    new Tuple<string[], bool>(new[] {"\t"}, true)
                };
        }

        public IEnumerable<Tuple<string[], string[], bool>> GivenInstance_WhenReadOutOldFile_ThenSuccess_Data()
        {
            return new[]
                {
                    new Tuple<string[], string[], bool>(new[] {"foo", "bar"}, new[] {"foo", "bar"}, true),
                    new Tuple<string[], string[], bool>(new[] {"foo", "bar", "baz"}, new[] {"foo", "bar"}, true),
                    new Tuple<string[], string[], bool>(new[] {"\t"}, new[] {"", ""}, true)
                };
        }

        [Test]
        [TestCase(0)]
        [TestCase(CSVReaderWriter.Mode.Read)]
        [TestCase(CSVReaderWriter.Mode.Write)]
        public void GivenInstance_WhenClosed_ThenSuccess(CSVReaderWriter.Mode mode)
        {
            var sut = new CSVReaderWriter();
            if ((int) mode > 0)
                sut.Open(CreateFile(), mode);

            sut.Close();

            Assert.Pass();
        }

        [Test]
        [TestCase(0, ExpectedException = typeof (Exception))]
        [TestCase(CSVReaderWriter.Mode.Read | CSVReaderWriter.Mode.Write, ExpectedException = typeof (Exception))]
        [TestCase(42, ExpectedException = typeof (Exception))]
        public void GivenInstance_WhenMixedOpenOldFile_ThenFail(CSVReaderWriter.Mode mode)
        {
            var sut = new CSVReaderWriter();

            sut.Open(CreateFile(), mode);

            Assert.Fail();
        }


        [Test, ExpectedException(typeof (FileNotFoundException))]
        public void GivenInstance_WhenOpenReadNewFile_ThenException()
        {
            var sut = new CSVReaderWriter();

            sut.Open(CreateFile() + "x", CSVReaderWriter.Mode.Read);

            Assert.Fail();
        }

        [Test]
        public void GivenInstance_WhenOpenReadOldFile_ThenSuccess()
        {
            var sut = new CSVReaderWriter();

            sut.Open(CreateFile(), CSVReaderWriter.Mode.Read);

            Assert.Pass();
        }


        [Test]
        public void GivenInstance_WhenOpenWriteNewFile_ThenSuccess()
        {
            var sut = new CSVReaderWriter();

            sut.Open(CreateFile() + "x", CSVReaderWriter.Mode.Write);

            Assert.Pass();
        }

        [Test]
        public void GivenInstance_WhenOpenWriteOldFile_ThenSuccess()
        {
            var sut = new CSVReaderWriter();

            sut.Open(CreateFile(), CSVReaderWriter.Mode.Write);

            Assert.Pass();
        }

        [Test]
        [TestCaseSource("GivenInstance_WhenReadOldFile_ThenSuccess_Data")]
        public void GivenInstance_WhenReadOldFile_ThenSuccess(Tuple<string[], bool> data)
        {
            var sut = new CSVReaderWriter();
            sut.Open(CreateFile(data.Item1), CSVReaderWriter.Mode.Read);

            bool result;
            try
            {
                result = sut.Read(null, null);
            }
            finally
            {
                sut.Close();
            }

            Assert.AreEqual(data.Item2, result, string.Format("Test data: {{{0}}}", string.Join("|", data.Item1)));
        }

        [Test]
        [TestCaseSource("GivenInstance_WhenReadOutOldFile_ThenSuccess_Data")]
        public void GivenInstance_WhenReadOutOldFile_ThenSuccess(Tuple<string[], string[], bool> data)
        {
            var sut = new CSVReaderWriter();
            sut.Open(CreateFile(data.Item1), CSVReaderWriter.Mode.Read);

            bool result;
            string column1;
            string column2;

            try
            {
                result = sut.Read(out column1, out column2);
            }
            finally
            {
                sut.Close();
            }

            string errorText = string.Format("Test data: {{{0}}}", string.Join("|", data.Item1));

            Assert.AreEqual(data.Item3, result, errorText);
            Assert.AreEqual(data.Item2[0], column1, errorText);
            Assert.AreEqual(data.Item2[1], column2, errorText);
        }

        [Test]
        [TestCaseSource("GivenInstance_WhenWriteOldFile_ThenSuccess_Data")]
        public void GivenInstance_WhenWriteOldFile_ThenSuccess(string[] what)
        {
            var sut = new CSVReaderWriter();
            sut.Open(CreateFile(), CSVReaderWriter.Mode.Write);

            try
            {
                sut.Write(what);
            }
            finally
            {
                sut.Close();
            }

            Assert.Pass();
        }

        [Test]
        public void GivenInstance_WhenReadInUsingBlock_ThenSuccess()
        {
            string[] result;

            using (var sut = new CSVReaderWriter(CreateFile("foo", "bar", "baz"), CSVReaderWriter.Mode.Read))
            {
                result = sut.ReadRecord().ToArray();
            }

            Assert.AreEqual(3, result.Length);
            Assert.AreEqual("foo", result[0]);
            Assert.AreEqual("bar", result[1]);
            Assert.AreEqual("baz", result[2]);
        }

        [Test]
        public void GivenInstance_WhenWriteInUsingBlock_ThenSuccess()
        {
            string file = CreateFile("nothing");

            using (var sut = new CSVReaderWriter(file, CSVReaderWriter.Mode.Write))
            {
                sut.Write("foo", "bar", "baz");
            }

            string result = File.OpenText(file).ReadToEnd();

            Assert.AreEqual("foo\tbar\tbaz\r\n", result);
        }

        // Stuff I am NOT testing for lack of time:
        // Calling two opens in a row (with the same or different modes, same or different file name...)
        // Calling read if in write mode and vice versa 
        // Using a library for proper escaping etc.
    }
}
// ReSharper restore InconsistentNaming
// ReSharper restore EmptyGeneralCatchClause
